from django.shortcuts import render, redirect, get_object_or_404
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.

# def home(request):
#     receipts = Receipt.objects.all()
#     context = {"receipts": receipts}
#     return render(request, "receipts/home.html", context)


# login_url="/accounts/login/"
@login_required()
def home(request):
    my_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": my_receipts}
    return render(request, "receipts/home.html", context)


def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


# @login_required
# def category_list(request):
#     my_receipts = Receipt.objects.filter(purchaser=request.user)
#     num_receipts_by_category = {}
#     for receipt in my_receipts:
#         if receipt.category.name not in num_receipts_by_category:
#             num_receipts_by_category[receipt.category.name] = 0
#         else:
#             num_receipts_by_category[receipt.category.name] += 1
#     context = {
#         "dct": num_receipts_by_category,
#     }
#     return render(request, "receipts/category_list.html", context)


# @login_required
# def account_list(request):
#     my_receipts = Receipt.objects.filter(purchaser=request.user)
#     num_receipts_by_account = {}
#     for receipt in my_receipts:
#         if receipt.account.name not in num_receipts_by_account:
#             num_receipts_by_account[receipt.account.name] = 0
#         else:
#             num_receipts_by_account[receipt.account.name] += 1
#     context = {
#         "dct": num_receipts_by_account,
#     }
#     return render(request, "receipts/account_list.html", context)


@login_required
def category_list(request):
    context = {
        "categories": request.user.categories.all(),
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    context = {
        "accounts": request.user.accounts.all(),
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
